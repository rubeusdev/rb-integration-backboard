<?php
namespace Rubeus\IntegracaoBackBoard;

class Config {
    public $HOSTNAME;
	public $KEY;
	public $SECRET;

	public $DSK_ID;
	public $AUTH_PATH;
	public $DSK_PATH;
	public $COURSE_PATH;
	public $USER_PATH;

    public function __construct(){
        $this->HOSTNAME = defined('BACKBOARD_HOSTNAME') ? BACKBOARD_HOSTNAME : '';
    	$this->KEY = defined('BACKBOARD_KEY') ? BACKBOARD_KEY : '';
    	$this->SECRET = defined('BACKBOARD_SECRET') ? BACKBOARD_SECRET : '';

    	$this->DSK_ID = defined('BACKBOARD_DSK_ID') ? BACKBOARD_DSK_ID : false;
    	$this->AUTH_PATH = defined('BACKBOARD_AUTH_PATH') ? BACKBOARD_AUTH_PATH : '';
    	$this->DSK_PATH = defined('BACKBOARD_DSK_PATH') ? BACKBOARD_DSK_PATH : '';
    	$this->COURSE_PATH = defined('BACKBOARD_COURSE_PATH') ? BACKBOARD_COURSE_PATH : '';
    	$this->USER_PATH = defined('BACKBOARD_USER_PATH') ? BACKBOARD_USER_PATH : '';
    }
}
