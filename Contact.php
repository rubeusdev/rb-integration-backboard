<?php
namespace Rubeus\IntegracaoBackBoard;

class Contact{
	public $homePhone = '';

	public $mobilePhone = '';

	public $businessPhone = '';

	public $businessFax = '';

	public $email = '';

	public $webPage = '';
}
