<?php
namespace Rubeus\IntegracaoBackBoard;

abstract class Error{
	const NOT_AUTHENTICATED = ['message' => 'Não autenticado.', 'code' => 0];
	const DATA_SOURE_NOT_FOUND = ['message' => 'Fonte de dados não encontrada.', 'code' => 1];
	const AUTHENTICATION_FAILED = ['message' => 'Falha ao realizar autenticação.', 'code' => 2];
	const CREATION_DATA_SOURCE_FAILED = ['message' => 'Falha ao criar fonte de dados.', 'code' => 4];
	const COURSE_NOT_FOUND = ['message' => 'Curso não encontrado', 'code' => 5];
	const CREATION_USER_FAILED = ['message' => 'Falha ao criar usuário.', 'code' => 6];
	const CREATION_MEMBERSHIP_FAILED = ['message' => 'Falha ao criar vinculo com o curso.', 'code' => 7];
}
