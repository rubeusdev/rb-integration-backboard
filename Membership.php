<?php
namespace Rubeus\IntegracaoBackBoard;

class Membership{
	public $userId = '';

	public $courseId = '';

	public $dataSourceId = '';

	public $created = '';

	public $availability;

	public $courseRoleId = 'Student';

	public function __construct(){
		$this->availability =  new Availability();
	}
}
