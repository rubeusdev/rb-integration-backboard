<?php
namespace Rubeus\IntegracaoBackBoard;
use Rubeus\ContenerDependencia\Conteiner;

class RestBackBoard {
	public $config;
	public $datasource;
	public $token;

    public function __construct(){
        $this->config = new Config();
    }

    public function setConfig($config){
        $this->config = $config;
    }

    private function response($request, $url, $data, $status=200){
		$statusResponse = $result = false;
		if($this->token && ( $this->config->DSK_ID || $status == 200  || strpos($url, $this->config->DSK_PATH))){
			$request->setHeader('Authorization', 'Bearer ' . $this->token->access_token);
	        try {
	            $response = $request->send();
	            $statusResponse = $response->getStatus();
	            $responseBody = $response->getBody();
	            if ($status == $statusResponse) {
	                $result = json_decode($responseBody);
	            }
	        } catch (\HTTP_Request2_Exception $e) {
	            $exception = $e->getMessage();
	        }
		}
		Conteiner::get('RegisterRequestBackBoard')->log($url, $data, $exception, $statusResponse, $responseBody);

		if(!$this->token){
			throw new ExceptionBackBoard(Error::NOT_AUTHENTICATED);
		}
		if(!($this->config->DSK_ID || $status == 200) && !strpos($url, $this->config->DSK_PATH)){
			throw new ExceptionBackBoard(Error::DATA_SOURE_NOT_FOUND);
		}
        return $result;
    }

	public function authorize() {
        $url = $this->config->HOSTNAME . $this->config->AUTH_PATH;
		$data = json_encode(['key' => $this->config->KEY, 'secret' => $this->config->SECRET]);

        $request = new \HTTP_Request2($this->config->HOSTNAME . $this->config->AUTH_PATH, \HTTP_Request2::METHOD_POST);
		$request->setAuth($this->config->KEY, $this->config->SECRET, \HTTP_Request2::AUTH_BASIC);
		$request->setBody('grant_type=client_credentials');
		$request->setHeader('Content-Type', 'application/x-www-form-urlencoded');
		$this->token = true;
		$this->token = $this->response($request, $url, $data);
		if($this->token){
			/*$this->readDatasource();
			if(!$this->datasoure){
				$datasoure = $this->createDatasource();
			}
			$this->config->DSK_ID = $this->datasoure->$id;*/
			$this->config->DSK_ID = '_149_1';
		}
		if(!$this->token){
			throw new ExceptionBackBoard(Error::AUTHENTICATION_FAILED);
		}
        return $this->token;
	}

	public function createDatasource() {
		$datasource = new Datasoure();
		$url = $this->config->HOSTNAME . $this->config->DSK_PATH;
		$data = json_encode($datasource);

		$request = new \HTTP_Request2($url, \HTTP_Request2::METHOD_POST);

		$request->setHeader('Content-Type', 'application/json');
		$request->setBody($data);

        $this->datasource = $this->response($request, $url, $data,201);
		if(!$this->datasource){
			throw new ExceptionBackBoard(Error::CREATION_DATA_SOURCE_FAILED);
		}
        return $this->datasource;
	}

	public function readDatasource() {
		$dataEnvio = array("externalId" => BACKBOARD_DSK_EXTERNAL_ID );
		$url = $this->config->HOSTNAME .  $this->config->DSK_PATH;
		$data = json_encode($dataEnvio);

		$request = new \HTTP_Request2($this->config->HOSTNAME . $this->config->DSK_PATH, \HTTP_Request2::METHOD_GET);

		$url = $request->getUrl();
		$url->setQueryVariables($dataEnvio);

		$this->datasource = $this->response($request, $url, $data);

        return $this->datasource;
	}

	public function readCourse($course_id) {
        $dataEnvio = array("courseId" => $course_id );
        $urlRequest = $this->config->HOSTNAME .  $this->config->COURSE_PATH;
        $data = json_encode($dataEnvio);

		$request = new \HTTP_Request2($urlRequest, \HTTP_Request2::METHOD_GET);

        $url = $request->getUrl();
		$url->setQueryVariables($dataEnvio);
		$course = $this->response($request, $urlRequest, $data);
		if(!$course){
			throw new ExceptionBackBoard(Error::COURSE_NOT_FOUND);
		}
		return  $course->results[0];
	}

	public function getUser($ra) {
        $dataEnvio = array("userName" => $ra );
        $urlRequest = $this->config->HOSTNAME .  $this->config->USER_PATH;
        $data = json_encode($dataEnvio);

		$request = new \HTTP_Request2($urlRequest, \HTTP_Request2::METHOD_GET);

        $url = $request->getUrl();
		$url->setQueryVariables($dataEnvio);
		$user = $this->response($request, $urlRequest, $data);

		if(!$user){
			throw new ExceptionBackBoard(Error::COURSE_NOT_FOUND);
		}
		return  $user->results[0];
	}

	public function createUser($user) {
		$user->dataSourceId =  $this->config->DSK_ID;

        $url = $this->config->HOSTNAME .  $this->config->USER_PATH;
        $data = json_encode($user);

		$request = new \HTTP_Request2($url, \HTTP_Request2::METHOD_POST);
		$request->setHeader('Content-Type', 'application/json');
		$request->setBody($data);

        $user = $this->response($request, $url, $data, 201);
		if(!$user){
			throw new ExceptionBackBoard(Error::CREATION_USER_FAILED);
		}
		return $user;
	}

	public function createMembership($course, $user) {
		$membership = new Membership();

		$membership->dataSourceId =  $this->config->DSK_ID;
		$membership->userId = $user->id;
		$membership->courseId = $course->id;

        $url = $this->config->HOSTNAME . $this->config->COURSE_PATH . '/' . $membership->courseId . '/users/' . $membership->userId;
        $data = json_encode($membership);

		$request = new \HTTP_Request2($url, \HTTP_Request2::METHOD_PUT);
		$request->setHeader('Content-Type', 'application/json');
		$request->setBody($data);

		$membership = $this->response($request, $url, $data, 201);
		if(!$membership){
			throw new ExceptionBackBoard(Error::CREATION_MEMBERSHIP_FAILED);
		}
		return $membership;
	}

	/**
	 * [getMembership Pesquisa o vinculo]
	 * @param  [type] $course [description]
	 * @param  [type] $user   [description]
	 * @return [type]         [description]
	 */
	public function getMembership($course,$user){

        $dataEnvio = array("courseId" => $course,"userId"=>$user);
        $urlRequest = $this->config->HOSTNAME .  $this->config->COURSE_PATH. '/' . $course . '/users/' . $user;
        $data = json_encode($dataEnvio);

        $request = new \HTTP_Request2($urlRequest, \HTTP_Request2::METHOD_GET);

        $url = $request->getUrl();
        $url->setQueryVariables($dataEnvio);
        $membership = $this->response($request, $urlRequest, $data);
        if(!$membership){
        	return false;
        }
        return  $membership->results[0];
	}

}
