<?php
namespace Rubeus\IntegracaoBackBoard;

class User{
	public $id = '';

	public $uuid = '';

	public $externalId = '';

	public $dataSourceId = '';

	public $userName = '';

    public $password = '';

	public $studentId = '';

	public $birthDate = '';

	public $created = '';

	public $lastLogin = '';

	public $availability;

	public $name;

	public $job = '';

	public $contact;

	public $address;

	public $locale = '';

	public function __construct(){
		$this->availability =  new Availability();
		$this->name =  new Name();
		$this->contact = new Contact();
		$this->address = new Address();
	}
}
