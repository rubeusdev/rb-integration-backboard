<?php
namespace Rubeus\IntegracaoBackBoard;

class ExceptionBackBoard  extends \Exception{
	private $erro;

	public function __construct($erro) {
        $this->erro = $erro;
        parent::__construct($erro['message'], $erro['code']);
    }
}
